# events-backend

## Jak się zalogować

1. Odinstaluj Postaman'a jeżeli jest zainstalowany.
2. Pobierz i zainstaluj [Insomnia REST Client](https://insomnia.rest/)
3. Z menu po lewej stronie klknij +, następnie New request.

![Alt text](https://i.ibb.co/1rwrc92/1.png)

4. Uzupełnij nowe okno w poniższy sposób i klinij Create

![Alt text](https://i.ibb.co/0sLjJz8/2.png)

5. Kliknij po lewej stronie w utworzony Request, uzupełnij pasek  adresu i pole pod paskiem adresu tak jak poniżej i kliknij Send. W odpowiedzi pojawi się JSON Web token.

![Alt text](https://i.ibb.co/6R74HP6/3.png)

6. Utwórz kolejny request tam jak w punkcie 3, ale o nazwie Protected (po prawej ma zostać domyślnie GET).
7. Kliknij na niego, wejdź w zakłądkę Auth, wybierz Bearer token. W polu Token wklej token otrzymany w kroku 5-tym, w polu Prefix wpisz Bearer. Upewnij się, że jest zaznaczone pole Enabled. W pasku adresu wpisz adres jak poniżej i kliknij Send:

![Alt text](https://i.ibb.co/wN9GP0B/4.png)



