package com.events.events.repository;

import com.events.events.model.entity.EventEntity;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

public interface EventRepository extends ReactiveMongoRepository<EventEntity, String> {
}
