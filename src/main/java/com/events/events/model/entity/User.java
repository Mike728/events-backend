package com.events.events.model.entity;

import com.events.events.model.enums.RoleType;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document
public class User {

    @Id
    private String id;

    private String email;

    private String firstName;

    private String lastName;

    private String password;

    private boolean active;

    private String registrationKey;

    private Organizer organizer;

    private List<RoleType> roleList;

    public User(String email, String password) {
        this.email = email;
        this.password = password;
        this.active = true;
    }

    public String getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPassword() {
        return password;
    }

    public boolean isActive() {
        return active;
    }

    public String getRegistrationKey() {
        return registrationKey;
    }

    public Organizer getOrganizer() {
        return organizer;
    }

    public List<RoleType> getRoleList() {
        return roleList;
    }


    public static final class UserBuilder {
        private String email;
        private String firstName;
        private String lastName;
        private String password;
        private boolean active;
        private String registrationKey;
        private Organizer organizer;
        private List<RoleType> roleList;

        private UserBuilder() {
        }

        public static UserBuilder anUser() {
            return new UserBuilder();
        }

        public UserBuilder withEmail(String email) {
            this.email = email;
            return this;
        }

        public UserBuilder withFirstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public UserBuilder withLastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public UserBuilder withPassword(String password) {
            this.password = password;
            return this;
        }

        public UserBuilder withActive(boolean active) {
            this.active = active;
            return this;
        }

        public UserBuilder withRegistrationKey(String registrationKey) {
            this.registrationKey = registrationKey;
            return this;
        }

        public UserBuilder withOrganizer(Organizer organizer) {
            this.organizer = organizer;
            return this;
        }

        public UserBuilder withRoleList(List<RoleType> roleList) {
            this.roleList = roleList;
            return this;
        }

        public User build() {
            User user = new User(email, password);
            user.firstName = this.firstName;
            user.roleList = this.roleList;
            user.organizer = this.organizer;
            user.lastName = this.lastName;
            user.active = this.active;
            user.registrationKey = this.registrationKey;
            return user;
        }
    }
}
