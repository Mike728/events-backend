package com.events.events.model.entity;

import com.events.events.model.enums.EventCategory;
import com.events.events.core.events.EventTransfer;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.List;

@Document
public class EventEntity {

    @Id
    private String id;
    private String eventUuid;
    private String name;
    private EventCategory category;
    private Organizer organizer; //class
    private String description;
    private byte[] photo;
    private LocalDateTime dateOfEvent;
    private EventLocation location;
    private List<String> tags;
    private Integer entriesAvailable;
    private Integer entriesSold;
    private List<String> participants;

    public EventEntity() {
    }

    public String getId() {
        return id;
    }

    public String getEventUuid() {
        return eventUuid;
    }

    public String getName() {
        return name;
    }

    public EventCategory getCategory() {
        return category;
    }

    public Organizer getOrganizer() {
        return organizer;
    }

    public String getDescription() {
        return description;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public LocalDateTime getDateOfEvent() {
        return dateOfEvent;
    }

    public EventLocation getLocation() {
        return location;
    }

    public List<String> getTags() {
        return tags;
    }

    public Integer getEntriesAvailable() {
        return entriesAvailable;
    }

    public Integer getEntriesSold() {
        return entriesSold;
    }

    public List<String> getParticipants() {
        return participants;
    }

    public static EventEntity fromTransferObject(EventTransfer eventTransfer) {
       return EventEntityBuilder.anEventEntity()
                .withCategory(eventTransfer.getCategory())
                .withDateOfEvent(eventTransfer.getDateOfEvent())
                .withDescription(eventTransfer.getDescription())
                .withEntriesAvailable(eventTransfer.getEntriesAvailable())
                .withEntriesSold(eventTransfer.getEntriesSold())
                .withEventUuid(eventTransfer.getEventUuid())
                .withLocation(eventTransfer.getLocation())
                .withName(eventTransfer.getName())
                .withOrganizer(eventTransfer.getOrganizer())
                .withParticipants(eventTransfer.getParticipants())
                .withPhoto(eventTransfer.getPhoto())
                .withTags(eventTransfer.getTags())
                .build();
    }

    public static final class EventEntityBuilder {
        private String eventUuid;
        private String name;
        private EventCategory category;
        private Organizer organizer; //class
        private String description;
        private byte[] photo;
        private LocalDateTime dateOfEvent;
        private EventLocation location;
        private List<String> tags;
        private Integer entriesAvailable;
        private Integer entriesSold;
        private List<String> participants;

        private EventEntityBuilder() {
        }

        public static EventEntityBuilder anEventEntity() {
            return new EventEntityBuilder();
        }

        public EventEntityBuilder withEventUuid(String eventUuid) {
            this.eventUuid = eventUuid;
            return this;
        }

        public EventEntityBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public EventEntityBuilder withCategory(EventCategory category) {
            this.category = category;
            return this;
        }

        public EventEntityBuilder withOrganizer(Organizer organizer) {
            this.organizer = organizer;
            return this;
        }

        public EventEntityBuilder withDescription(String description) {
            this.description = description;
            return this;
        }

        public EventEntityBuilder withPhoto(byte[] photo) {
            this.photo = photo;
            return this;
        }

        public EventEntityBuilder withDateOfEvent(LocalDateTime dateOfEvent) {
            this.dateOfEvent = dateOfEvent;
            return this;
        }

        public EventEntityBuilder withLocation(EventLocation location) {
            this.location = location;
            return this;
        }

        public EventEntityBuilder withTags(List<String> tags) {
            this.tags = tags;
            return this;
        }

        public EventEntityBuilder withEntriesAvailable(Integer entriesAvailable) {
            this.entriesAvailable = entriesAvailable;
            return this;
        }

        public EventEntityBuilder withEntriesSold(Integer entriesSold) {
            this.entriesSold = entriesSold;
            return this;
        }

        public EventEntityBuilder withParticipants(List<String> participants) {
            this.participants = participants;
            return this;
        }

        public EventEntity build() {
            EventEntity eventEntity = new EventEntity();
            eventEntity.organizer = this.organizer;
            eventEntity.participants = this.participants;
            eventEntity.location = this.location;
            eventEntity.name = this.name;
            eventEntity.category = this.category;
            eventEntity.tags = this.tags;
            eventEntity.dateOfEvent = this.dateOfEvent;
            eventEntity.entriesAvailable = this.entriesAvailable;
            eventEntity.eventUuid = this.eventUuid;
            eventEntity.description = this.description;
            eventEntity.entriesSold = this.entriesSold;
            eventEntity.photo = this.photo;
            return eventEntity;
        }
    }
}
