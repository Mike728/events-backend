package com.events.events.model.entity;

public class Organizer {

    private String firstName;
    private String surname;
    private String companyName;
    private String description;
    private String phone;
    private String email;
    private String domain;
    private String facebook;
    private byte[] logo;

    public Organizer() {
    }

    public String getFirstName() {
        return firstName;
    }

    public String getSurname() {
        return surname;
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getDescription() {
        return description;
    }

    public String getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }

    public String getDomain() {
        return domain;
    }

    public String getFacebook() {
        return facebook;
    }

    public byte[] getLogo() {
        return logo;
    }

    public static final class OrganizerBuilder {
        private String firstName;
        private String surname;
        private String companyName;
        private String description;
        private String phone;
        private String email;
        private String domain;
        private String facebook;
        private byte[] logo;

        private OrganizerBuilder() {
        }

        public static OrganizerBuilder anOrganizer() {
            return new OrganizerBuilder();
        }

        public OrganizerBuilder withFirstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public OrganizerBuilder withSurname(String surname) {
            this.surname = surname;
            return this;
        }

        public OrganizerBuilder withCompanyName(String companyName) {
            this.companyName = companyName;
            return this;
        }

        public OrganizerBuilder withDescription(String description) {
            this.description = description;
            return this;
        }

        public OrganizerBuilder withPhone(String phone) {
            this.phone = phone;
            return this;
        }

        public OrganizerBuilder withEmail(String email) {
            this.email = email;
            return this;
        }

        public OrganizerBuilder withDomain(String domain) {
            this.domain = domain;
            return this;
        }

        public OrganizerBuilder withFacebook(String facebook) {
            this.facebook = facebook;
            return this;
        }

        public OrganizerBuilder withLogo(byte[] logo) {
            this.logo = logo;
            return this;
        }

        public Organizer build() {
            Organizer organizer = new Organizer();
            organizer.logo = this.logo;
            organizer.facebook = this.facebook;
            organizer.phone = this.phone;
            organizer.email = this.email;
            organizer.companyName = this.companyName;
            organizer.domain = this.domain;
            organizer.surname = this.surname;
            organizer.description = this.description;
            organizer.firstName = this.firstName;
            return organizer;
        }
    }
}
