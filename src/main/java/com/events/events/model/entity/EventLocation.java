package com.events.events.model.entity;

public class EventLocation {

    private String location;
    private String street;
    private String streetNumber;
    private String city;

    public EventLocation() {
    }

    public String getLocation() {
        return location;
    }

    public String getStreet() {
        return street;
    }

    public String getStreetNumber() {
        return streetNumber;
    }

    public String getCity() {
        return city;
    }

    public static final class EventLocationBuilder {
        private String location;
        private String street;
        private String streetNumber;
        private String city;

        private EventLocationBuilder() {
        }

        public static EventLocationBuilder anEventLocation() {
            return new EventLocationBuilder();
        }

        public EventLocationBuilder withLocation(String location) {
            this.location = location;
            return this;
        }

        public EventLocationBuilder withStreet(String street) {
            this.street = street;
            return this;
        }

        public EventLocationBuilder withStreetNumber(String streetNumber) {
            this.streetNumber = streetNumber;
            return this;
        }

        public EventLocationBuilder withCity(String city) {
            this.city = city;
            return this;
        }

        public EventLocation build() {
            EventLocation eventLocation = new EventLocation();
            eventLocation.city = this.city;
            eventLocation.street = this.street;
            eventLocation.streetNumber = this.streetNumber;
            eventLocation.location = this.location;
            return eventLocation;
        }
    }
}
