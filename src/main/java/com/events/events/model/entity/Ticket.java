package com.events.events.model.entity;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class Ticket {

    private String name;
    private String type;
    private BigDecimal price;
    private LocalDateTime sellingFrom;
    private LocalDateTime sellingTo;
    private Integer minBuyAtOnce;
    private Integer maxBuyAtOnce;
    private String description;
    private String ticketText;
    private String typeOfTickets;

    public Ticket() {
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public LocalDateTime getSellingFrom() {
        return sellingFrom;
    }

    public LocalDateTime getSellingTo() {
        return sellingTo;
    }

    public Integer getMinBuyAtOnce() {
        return minBuyAtOnce;
    }

    public Integer getMaxBuyAtOnce() {
        return maxBuyAtOnce;
    }

    public String getDescription() {
        return description;
    }

    public String getTicketText() {
        return ticketText;
    }

    public String getTypeOfTickets() {
        return typeOfTickets;
    }

    public static final class TicketBuilder {
        private String name;
        private String type;
        private BigDecimal price;
        private LocalDateTime sellingFrom;
        private LocalDateTime sellingTo;
        private Integer minBuyAtOnce;
        private Integer maxBuyAtOnce;
        private String description;
        private String ticketText;
        private String typeOfTickets;

        private TicketBuilder() {
        }

        public static TicketBuilder aTicket() {
            return new TicketBuilder();
        }

        public TicketBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public TicketBuilder withType(String type) {
            this.type = type;
            return this;
        }

        public TicketBuilder withPrice(BigDecimal price) {
            this.price = price;
            return this;
        }

        public TicketBuilder withSellingFrom(LocalDateTime sellingFrom) {
            this.sellingFrom = sellingFrom;
            return this;
        }

        public TicketBuilder withSellingTo(LocalDateTime sellingTo) {
            this.sellingTo = sellingTo;
            return this;
        }

        public TicketBuilder withMinBuyAtOnce(Integer minBuyAtOnce) {
            this.minBuyAtOnce = minBuyAtOnce;
            return this;
        }

        public TicketBuilder withMaxBuyAtOnce(Integer maxBuyAtOnce) {
            this.maxBuyAtOnce = maxBuyAtOnce;
            return this;
        }

        public TicketBuilder withDescription(String description) {
            this.description = description;
            return this;
        }

        public TicketBuilder withTicketText(String ticketText) {
            this.ticketText = ticketText;
            return this;
        }

        public TicketBuilder withTypeOfTickets(String typeOfTickets) {
            this.typeOfTickets = typeOfTickets;
            return this;
        }

        public Ticket build() {
            Ticket ticket = new Ticket();
            ticket.price = this.price;
            ticket.maxBuyAtOnce = this.maxBuyAtOnce;
            ticket.ticketText = this.ticketText;
            ticket.name = this.name;
            ticket.description = this.description;
            ticket.typeOfTickets = this.typeOfTickets;
            ticket.sellingFrom = this.sellingFrom;
            ticket.minBuyAtOnce = this.minBuyAtOnce;
            ticket.type = this.type;
            ticket.sellingTo = this.sellingTo;
            return ticket;
        }
    }
}
