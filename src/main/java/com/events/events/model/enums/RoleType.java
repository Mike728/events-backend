package com.events.events.model.enums;

public enum RoleType {
    USER,
    ORGANIZER,
    ADMIN
}
