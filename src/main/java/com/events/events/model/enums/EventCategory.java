package com.events.events.model.enums;

public enum EventCategory {

    TRAINING("Szkolenie"),
    CONFERENCE("Konferencja"),
    CONCERT("Koncert"),
    PARTY("Impreza"),
    CULTURE("Kultura"),
    SPORT("Sport"),
    FILM("Film");

    String description;

    EventCategory(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
