package com.events.events.bootstrap;

import com.events.events.model.entity.EventEntity;
import com.events.events.model.entity.EventLocation;
import com.events.events.model.entity.Organizer;
import com.events.events.model.entity.User;
import com.events.events.model.enums.EventCategory;
import com.events.events.repository.EventRepository;
import com.events.events.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@Component
public class DataInitializer implements ApplicationListener<ContextRefreshedEvent> {

    private EventRepository eventRepository;
    private UserRepository userRepository;

    private Logger LOGGER = LoggerFactory.getLogger(DataInitializer.class);

    public DataInitializer(EventRepository eventRepository, UserRepository userRepository) {
        this.eventRepository = eventRepository;
        this.userRepository = userRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
//        userRepository.deleteAll().subscribe();
//        eventRepository.deleteAll().subscribe();
//        initializeEvents().subscribe(event -> LOGGER.info("Saved event with UUID: " + event.getEventUuid()));
//        initializeUsers().subscribe(user -> LOGGER.info("Saved user with login: " + user.getEmail()));
    }

    private Flux<EventEntity> initializeEvents() {

        Organizer organizer = Organizer.OrganizerBuilder.anOrganizer()
            .withCompanyName("Facebook")
            .withDescription("Serwis społecznościowy")
            .withDomain("https://www.facebook.com")
            .withEmail("facebook@facebook.com")
            .withFacebook("facebook")
            .withFirstName("Krzysztof")
            .withPhone("123-456-789")
            .withSurname("Murawski")
            .build();

        EventLocation eventLocation = EventLocation.EventLocationBuilder.anEventLocation()
            .withCity("Toronto")
            .withLocation("Ontario")
            .withStreetNumber("123a")
            .withStreet("Danforth street")
            .build();

        EventEntity eventEntity = EventEntity.EventEntityBuilder.anEventEntity()
            .withCategory(EventCategory.TRAINING)
            .withDateOfEvent(LocalDateTime.now().plusMonths(9))
            .withDescription("Szkolenie z facebook'a")
            .withEntriesAvailable(10)
            .withEntriesSold(33)
            .withEventUuid(UUID.randomUUID().toString())
            .withName("Szkolenie facebook")
            .withTags(Collections.unmodifiableList(Arrays.asList("facebook")))
            .withLocation(eventLocation)
            .withOrganizer(organizer)
            .build();

        Organizer organizer2 = Organizer.OrganizerBuilder.anOrganizer()
            .withCompanyName("Lidl")
            .withDescription("Siec sklepów")
            .withDomain("https://www.lidl.pl")
            .withEmail("lidl@lidl.pl")
            .withFacebook("Lidl polska")
            .withFirstName("Janusz")
            .withPhone("123-456-789")
            .withSurname("Kot")
            .build();

        EventLocation eventLocation2 = EventLocation.EventLocationBuilder.anEventLocation()
            .withCity("Poznań")
            .withLocation("Centrum")
            .withStreetNumber("72a")
            .withStreet("Zwycięstwa")
            .build();

        EventEntity eventEntity2 = EventEntity.EventEntityBuilder.anEventEntity()
            .withCategory(EventCategory.CONFERENCE)
            .withDateOfEvent(LocalDateTime.now().plusMonths(9))
            .withDescription("Szkolenie z lidl'a")
            .withEntriesAvailable(5)
            .withEntriesSold(20)
            .withEventUuid(UUID.randomUUID().toString())
            .withName("Szkolenie Lidl Polska")
            .withTags(Collections.unmodifiableList(Arrays.asList("lidl", "polska")))
            .withLocation(eventLocation2)
            .withOrganizer(organizer2)
            .build();

        List<EventEntity> eventEntities = Arrays.asList(eventEntity, eventEntity2);

        return eventRepository.saveAll(eventEntities);
    }

    private Flux<User> initializeUsers() {
        User user = new User("admin", "password");

        List<User> users = Arrays.asList(user);

        return userRepository.saveAll(users);
    }
}
