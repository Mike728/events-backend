package com.events.events.core.events;

import com.events.events.filtering.Filter;
import com.events.events.model.entity.EventEntity;
import com.events.events.repository.EventRepository;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

@Service
public class EventService {

    private final EventRepository eventRepository;

    public EventService(EventRepository eventRepository) {
        this.eventRepository = eventRepository;
    }

    public Mono<EventTransfer> saveEvent(EventTransfer eventTransfer) {
        EventEntity eventEntity = EventEntity.fromTransferObject(eventTransfer);

        return eventRepository.save(eventEntity)
                .map(EventTransfer::fromTransferObject);
    }

    public Flux<EventEntity> filter(List<Filter> filters) {
        return eventRepository.findAll()
            .compose(eventsFlux -> meetAllFilters(eventsFlux, filters));
    }

    private Flux<EventEntity> meetAllFilters(Flux<EventEntity> entityFlux, List<Filter> allFilters) {
        Flux<EventEntity> filteredEventFlux = entityFlux;

        for (Filter filter : allFilters) {
            filteredEventFlux = filter.meetFilter(filteredEventFlux);
        }

        return filteredEventFlux;
    }
}