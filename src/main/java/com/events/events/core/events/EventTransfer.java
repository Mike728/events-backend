package com.events.events.core.events;

import com.events.events.model.entity.EventEntity;
import com.events.events.model.entity.EventLocation;
import com.events.events.model.entity.Organizer;
import com.events.events.model.enums.EventCategory;

import java.time.LocalDateTime;
import java.util.List;

public class EventTransfer {

    private String id;
    private String eventUuid;
    private String name;
    private EventCategory category;
    private Organizer organizer; //class
    private String description;
    private byte[] photo;
    private LocalDateTime dateOfEvent;
    private EventLocation location;
    private List<String> tags;

    private Integer entriesAvailable;
    private Integer entriesSold;
    private List<String> participants;

    public EventTransfer() {
    }

    public String getId() {
        return id;
    }

    public String getEventUuid() {
        return eventUuid;
    }

    public String getName() {
        return name;
    }

    public EventCategory getCategory() {
        return category;
    }

    public Organizer getOrganizer() {
        return organizer;
    }

    public String getDescription() {
        return description;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public LocalDateTime getDateOfEvent() {
        return dateOfEvent;
    }

    public EventLocation getLocation() {
        return location;
    }

    public List<String> getTags() {
        return tags;
    }

    public Integer getEntriesAvailable() {
        return entriesAvailable;
    }

    public Integer getEntriesSold() {
        return entriesSold;
    }

    public List<String> getParticipants() {
        return participants;
    }

    public static EventTransfer fromTransferObject(EventEntity eventEntity) {
       return EventTransferBuilder.anEventTransfer()
                .withCategory(eventEntity.getCategory())
                .withDateOfEvent(eventEntity.getDateOfEvent())
                .withDescription(eventEntity.getDescription())
                .withEntriesAvailable(eventEntity.getEntriesAvailable())
                .withEntriesSold(eventEntity.getEntriesSold())
                .withEventUuid(eventEntity.getEventUuid())
                .withId(eventEntity.getId())
                .withLocation(eventEntity.getLocation())
                .withName(eventEntity.getName())
                .withOrganizer(eventEntity.getOrganizer())
                .withParticipants(eventEntity.getParticipants())
                .withPhoto(eventEntity.getPhoto())
                .withTags(eventEntity.getTags())
                .build();
    }


    public static final class EventTransferBuilder {
        private String id;
        private String eventUuid;
        private String name;
        private EventCategory category;
        private Organizer organizer; //class
        private String description;
        private byte[] photo;
        private LocalDateTime dateOfEvent;
        private EventLocation location;
        private List<String> tags;
        private Integer entriesAvailable;
        private Integer entriesSold;
        private List<String> participants;

        private EventTransferBuilder() {
        }

        public static EventTransferBuilder anEventTransfer() {
            return new EventTransferBuilder();
        }

        public EventTransferBuilder withId(String id) {
            this.id = id;
            return this;
        }

        public EventTransferBuilder withEventUuid(String eventUuid) {
            this.eventUuid = eventUuid;
            return this;
        }

        public EventTransferBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public EventTransferBuilder withCategory(EventCategory category) {
            this.category = category;
            return this;
        }

        public EventTransferBuilder withOrganizer(Organizer organizer) {
            this.organizer = organizer;
            return this;
        }

        public EventTransferBuilder withDescription(String description) {
            this.description = description;
            return this;
        }

        public EventTransferBuilder withPhoto(byte[] photo) {
            this.photo = photo;
            return this;
        }

        public EventTransferBuilder withDateOfEvent(LocalDateTime dateOfEvent) {
            this.dateOfEvent = dateOfEvent;
            return this;
        }

        public EventTransferBuilder withLocation(EventLocation location) {
            this.location = location;
            return this;
        }

        public EventTransferBuilder withTags(List<String> tags) {
            this.tags = tags;
            return this;
        }

        public EventTransferBuilder withEntriesAvailable(Integer entriesAvailable) {
            this.entriesAvailable = entriesAvailable;
            return this;
        }

        public EventTransferBuilder withEntriesSold(Integer entriesSold) {
            this.entriesSold = entriesSold;
            return this;
        }

        public EventTransferBuilder withParticipants(List<String> participants) {
            this.participants = participants;
            return this;
        }

        public EventTransfer build() {
            EventTransfer eventTransfer = new EventTransfer();
            eventTransfer.name = this.name;
            eventTransfer.eventUuid = this.eventUuid;
            eventTransfer.dateOfEvent = this.dateOfEvent;
            eventTransfer.location = this.location;
            eventTransfer.id = this.id;
            eventTransfer.entriesSold = this.entriesSold;
            eventTransfer.participants = this.participants;
            eventTransfer.tags = this.tags;
            eventTransfer.category = this.category;
            eventTransfer.organizer = this.organizer;
            eventTransfer.description = this.description;
            eventTransfer.entriesAvailable = this.entriesAvailable;
            eventTransfer.photo = this.photo;
            return eventTransfer;
        }
    }
}
