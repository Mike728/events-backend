package com.events.events.core.events;

import com.events.events.filtering.events.EventsFilteringService;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/api/events")
public class EventController {

    private final EventsFilteringService eventsFilteringService;
    private final EventService eventService;

    public EventController(EventsFilteringService eventsFilteringService, EventService eventService) {
        this.eventsFilteringService = eventsFilteringService;
        this.eventService = eventService;
    }

    @GetMapping
    public Flux<EventTransfer> findEventsByParameters(ServerWebExchange serverWebExchange) {
        MultiValueMap<String, String> queryParams = serverWebExchange.getRequest().getQueryParams();

        return eventService
                .filter(eventsFilteringService.initFilters(queryParams))
                .map(EventTransfer::fromTransferObject);
    }

    @PostMapping
    public Mono<EventTransfer> createEvent(@RequestBody EventTransfer eventTransfer) {
        return eventService.saveEvent(eventTransfer);
    }
}
