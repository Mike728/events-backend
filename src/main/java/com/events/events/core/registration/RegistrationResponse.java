package com.events.events.core.registration;

public class RegistrationResponse {

    private String email;
    private boolean active;
    private boolean registered;

    public RegistrationResponse() {
    }

    public RegistrationResponse(String email, boolean active, boolean registered) {
        this.email = email;
        this.active = active;
        this.registered = registered;
    }

    public String getEmail() {
        return email;
    }

    public boolean isActive() {
        return active;
    }

    public boolean isRegistered() {
        return registered;
    }
}
