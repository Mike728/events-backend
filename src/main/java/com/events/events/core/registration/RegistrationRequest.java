package com.events.events.core.registration;

public class RegistrationRequest {

    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private String repeatPassword;

    public RegistrationRequest() {
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getRepeatPassword() {
        return repeatPassword;
    }


    public static final class RegistrationRequestBuilder {
        private String firstName;
        private String lastName;
        private String email;
        private String password;
        private String repeatPassword;

        private RegistrationRequestBuilder() {
        }

        public static RegistrationRequestBuilder aRegistrationRequest() {
            return new RegistrationRequestBuilder();
        }

        public RegistrationRequestBuilder withFirstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public RegistrationRequestBuilder withLastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public RegistrationRequestBuilder withEmail(String email) {
            this.email = email;
            return this;
        }

        public RegistrationRequestBuilder withPassword(String password) {
            this.password = password;
            return this;
        }

        public RegistrationRequestBuilder withRepeatPassword(String repeatPassword) {
            this.repeatPassword = repeatPassword;
            return this;
        }

        public RegistrationRequest build() {
            RegistrationRequest registrationRequest = new RegistrationRequest();
            registrationRequest.email = this.email;
            registrationRequest.firstName = this.firstName;
            registrationRequest.repeatPassword = this.repeatPassword;
            registrationRequest.lastName = this.lastName;
            registrationRequest.password = this.password;
            return registrationRequest;
        }
    }
}
