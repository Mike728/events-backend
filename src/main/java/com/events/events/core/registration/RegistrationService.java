package com.events.events.core.registration;

import com.events.events.model.entity.User;
import com.events.events.model.enums.RoleType;
import com.events.events.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.UUID;
import java.util.function.Function;

@Service
public class RegistrationService {

    private final UserRepository userRepository;

    private final Logger LOG = LoggerFactory.getLogger(RegistrationService.class);

    public RegistrationService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public Mono<RegistrationResponse> registerUser(RegistrationRequest registrationRequest) {
        String email = registrationRequest.getEmail();

        return userRepository
                .findByEmail(email)
                .hasElement()
                .flatMap(isPresent -> {
                    if (isPresent) {
                        LOG.info("User with email: " + email + " has not been registered");
                        return Mono.just(new RegistrationResponse(email, false, false));
                    }

                    return saveRegistrationRequestAsUser(registrationRequest)
                            .map(user -> {
                                LOG.info("User with email: " + email + " has been registered");
                                return new RegistrationResponse(email, false, true);
                            });
                });
    }

    private Mono<User> saveRegistrationRequestAsUser(RegistrationRequest registrationRequest) {
        return Mono.fromSupplier(() -> userRepository
                .save(mapRegistrationRequestToUserEntity(registrationRequest)))
                .flatMap(Function.identity());
    }

    private User mapRegistrationRequestToUserEntity(RegistrationRequest registrationRequest) {
        return User.UserBuilder.anUser()
                .withActive(true) //ToDo email activation
                .withEmail(registrationRequest.getEmail())
                .withFirstName(registrationRequest.getFirstName())
                .withLastName(registrationRequest.getLastName())
                .withPassword(registrationRequest.getPassword())
                .withRegistrationKey(UUID.randomUUID().toString())
                .withRoleList(Arrays.asList(RoleType.USER))
                .build();
    }
}
