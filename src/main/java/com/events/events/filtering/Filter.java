package com.events.events.filtering;

import reactor.core.publisher.Flux;

import java.util.List;

public interface Filter<T> {

    Flux<T> meetFilter(Flux<T> eventsFlux);

    List<String> getValue();

}
