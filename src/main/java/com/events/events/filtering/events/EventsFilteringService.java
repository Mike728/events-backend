package com.events.events.filtering.events;

import com.events.events.common.AbstractFactory;
import com.events.events.common.FactoryName;
import com.events.events.common.FactoryProvider;
import com.events.events.filtering.Filter;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class EventsFilteringService {

    public List<Filter> initFilters(MultiValueMap<String, String> queryParams) {
        AbstractFactory<Filter> filterFactory = FactoryProvider.getFactory(FactoryName.FILTER);

        return queryParams.entrySet()
                .stream()
                .map(entry -> filterFactory.create(entry.getKey(), entry.getValue()))
                .collect(Collectors.toList());
    }
}
