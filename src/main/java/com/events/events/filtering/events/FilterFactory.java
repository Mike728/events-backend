package com.events.events.filtering.events;

import com.events.events.common.AbstractFactory;
import com.events.events.filtering.Filter;
import com.events.events.filtering.events.filters.CategoryFilter;
import com.events.events.filtering.events.filters.DescriptionFilter;
import com.events.events.filtering.events.filters.NameFilter;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.stream.Collectors;

public class FilterFactory implements AbstractFactory<Filter> {

    @Override
    public Filter create(String name, List<String> value) {

        value = toLowerCase(value);

        if(name.equals("name")) {
            return new NameFilter(value);
        } else if (name.equals("description")) {
            return new DescriptionFilter(value);
        } else if (name.equals("category")) {
            return new CategoryFilter(value);
        }
        return null;
    }

    @NotNull
    private List<String> toLowerCase(List<String> value) {
        return value
                .stream()
                .map(String::toLowerCase)
                .collect(Collectors.toList());
    }
}
