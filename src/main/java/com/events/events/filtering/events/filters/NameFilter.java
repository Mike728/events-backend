package com.events.events.filtering.events.filters;

import com.events.events.filtering.Filter;
import com.events.events.model.entity.EventEntity;
import org.jetbrains.annotations.NotNull;
import reactor.core.publisher.Flux;

import java.util.List;
import java.util.function.Predicate;

public class NameFilter implements Filter<EventEntity> {

    private List<String> nameList;

    public NameFilter(List<String> nameList) {
        this.nameList = nameList;
    }

    @Override
    public Flux<EventEntity> meetFilter(Flux<EventEntity> eventsFlux) { //ToDO null handle
        return eventsFlux.filter(eventEntity ->
                nameList.stream().anyMatch(namePredicate(eventEntity)));
    }

    @NotNull
    private Predicate<String> namePredicate(EventEntity eventEntity) {
        return nameEntry -> eventEntity.getName().toLowerCase().contains(nameEntry);
    }

    @Override
    public List<String> getValue() {
        return nameList;
    }
}
