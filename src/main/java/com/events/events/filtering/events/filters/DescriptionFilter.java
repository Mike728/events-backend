package com.events.events.filtering.events.filters;

import com.events.events.filtering.Filter;
import com.events.events.model.entity.EventEntity;
import org.jetbrains.annotations.NotNull;
import reactor.core.publisher.Flux;

import java.util.List;
import java.util.function.Predicate;

public class DescriptionFilter implements Filter<EventEntity> {

    private List<String> descriptionList;

    public DescriptionFilter(List<String> descriptionList) {
        this.descriptionList = descriptionList;
    }

    @Override
    public Flux<EventEntity> meetFilter(Flux<EventEntity> eventsFlux) { //ToDO null handle
        return eventsFlux.filter(eventEntity ->
                descriptionList.stream().anyMatch(descriptionPredicate(eventEntity)));
    }

    @NotNull
    private Predicate<String> descriptionPredicate(EventEntity eventEntity) {
        return descriptionEntry -> eventEntity.getDescription().toLowerCase().contains(descriptionEntry);
    }

    @Override
    public List<String> getValue() {
        return null;
    }
}
