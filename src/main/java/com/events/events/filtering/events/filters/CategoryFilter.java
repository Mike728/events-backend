package com.events.events.filtering.events.filters;

import com.events.events.filtering.Filter;
import com.events.events.model.entity.EventEntity;
import org.jetbrains.annotations.NotNull;
import reactor.core.publisher.Flux;

import java.util.List;
import java.util.function.Predicate;

public class CategoryFilter implements Filter<EventEntity> {

    private List<String> categoryList;

    public CategoryFilter(List<String> categoryList) {
        this.categoryList = categoryList;
    }

    @Override
    public Flux<EventEntity> meetFilter(Flux<EventEntity> eventsFlux) { //ToDo null handle
        return eventsFlux.filter(eventEntity ->
                categoryList.stream().anyMatch(categoryPredicate(eventEntity)));
    }

    @NotNull
    private Predicate<String> categoryPredicate(EventEntity eventEntity) {
        return categoryEntry -> eventEntity.getCategory().getDescription().toLowerCase().contains(categoryEntry);
    }

    @Override
    public List<String> getValue() {
        return categoryList;
    }
}
