package com.events.events.security;

import com.events.events.repository.UserRepository;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.util.Collections;

public class AuthenticationProvider implements ReactiveAuthenticationManager {

    private final TokenService tokenService;
    private final UserRepository userRepository;

    public AuthenticationProvider(TokenService tokenService, UserRepository userRepository) {
        this.tokenService = tokenService;
        this.userRepository = userRepository;
    }

    @Override
    public Mono<Authentication> authenticate(Authentication authentication) {
        String name = authentication.getName();
        String password = authentication.getCredentials().toString();

        return verifyThatUserExistInDatabase(name, password)
            .map(auth -> tokenService.generateToken(auth, auth.getAuthorities()));
    }

    private Mono<Authentication> verifyThatUserExistInDatabase(String email, String password){
        return userRepository.findByEmail(email)
            .switchIfEmpty(Mono.error(new IllegalArgumentException("User not found")))
            .flatMap(user -> {
                if (user.getPassword().equals(password) && user.isActive() == true) {
                    return Mono.just(new UsernamePasswordAuthenticationToken(email, password, Collections.emptyList()));
                }
                return Mono.error(new IllegalArgumentException("Invalid username or password"));
            });
    }
}
