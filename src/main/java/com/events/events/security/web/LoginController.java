package com.events.events.security.web;

import com.events.events.common.TokenUtils;
import com.events.events.security.AuthenticationProvider;
import com.events.events.security.ReactiveTokenRepository;
import com.events.events.security.dto.LoginRequest;
import com.events.events.security.dto.LoginResponse;
import com.events.events.security.dto.TokenIntrospectionResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.Collections;

@RestController
@RequestMapping("/api")
public class LoginController {
    private Logger logger = LoggerFactory.getLogger(LoginController.class);

    //TODO te zaleznosci zaraz do wywalenia
    private final AuthenticationProvider authenticationProvider;
    private final ReactiveTokenRepository reactiveTokenRepository;

    public LoginController(AuthenticationProvider authenticationProvider, ReactiveTokenRepository reactiveTokenRepository) {
        this.authenticationProvider = authenticationProvider;
        this.reactiveTokenRepository = reactiveTokenRepository;
    }

    @PostMapping(value = "/login")
    public Mono<LoginResponse> loginRequest(@RequestBody LoginRequest loginRequest, ServerWebExchange serverWebExchange){
        String principal = loginRequest.getLogin();

        logger.info("Received login request: " + loginRequest);
        return authenticationProvider
            .authenticate(new UsernamePasswordAuthenticationToken(principal, loginRequest.getPassword(), Collections.EMPTY_LIST))
            .flatMap(authentication -> reactiveTokenRepository
                .save(serverWebExchange, new SecurityContextImpl(authentication))
                .then(Mono.just(authentication.getCredentials().toString())))
            .map(LoginResponse::new);
    }

    @GetMapping("/introspect")
    public Mono<TokenIntrospectionResponse> introspectToken(ServerWebExchange serverWebExchange) {
        String authorization = serverWebExchange.getRequest().getHeaders().getFirst("Authorization");
        String token = TokenUtils.extractToken(authorization);

        return Mono.just(new TokenIntrospectionResponse(TokenUtils.validateToken(token)));
    }

}
