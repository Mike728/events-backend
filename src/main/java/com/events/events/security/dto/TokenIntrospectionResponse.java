package com.events.events.security.dto;

public class TokenIntrospectionResponse {

    private boolean active;

    public TokenIntrospectionResponse(boolean active) {
        this.active = active;
    }

    public boolean isActive() {
        return active;
    }
}
