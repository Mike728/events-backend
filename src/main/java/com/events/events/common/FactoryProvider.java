package com.events.events.common;

import com.events.events.filtering.events.FilterFactory;

public class FactoryProvider {

    public static AbstractFactory getFactory(FactoryName factoryName){
        if(factoryName.equals(FactoryName.FILTER)){
            return new FilterFactory();
        }
        return null;
    }
}
