package com.events.events.common;

import java.util.List;

public interface AbstractFactory<T> {

    T create(String name, List<String> value);
}
