package com.events.events.common;

import com.events.events.security.SecurityConstants;
import io.jsonwebtoken.*;
import org.apache.logging.log4j.util.Strings;

import java.nio.charset.StandardCharsets;
import java.sql.Date;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Collections;

public class TokenUtils {

    static public boolean validateToken(String token) {
        try {
            Jwts.parser()
                .setSigningKey(SecurityConstants.KEY.getBytes(StandardCharsets.UTF_8))
                .parseClaimsJws(token);
        } catch (ExpiredJwtException e) {
            e.printStackTrace();
        } catch (UnsupportedJwtException e) {
            e.printStackTrace();
        } catch (MalformedJwtException e) {
            e.printStackTrace();
        } catch (SignatureException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }

        return true;
    }

    static public String extractToken(String token) {
        if(token != null && Strings.isNotEmpty(token)){
            token = token.replace("Bearer", "");
        }
        return token;
    }

    static public String createTestToken() {
        return Jwts.builder()
            .setSubject("test")
            .addClaims(Collections.emptyMap())
            .setExpiration(Date.from(LocalDateTime.now()
                .plusSeconds(60L)
                .atZone(ZoneId.systemDefault())
                .toInstant()))
            .signWith(SignatureAlgorithm.HS512, SecurityConstants.KEY.getBytes(StandardCharsets.UTF_8))
            .compact();
    }
}
