package com.events.events.web;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

@RestController
public class TestProtectedController {

    @GetMapping("/protected")
    public Flux<Integer> accessWithJWTOnly() {
        return Flux.just(1,2,3);
    }
}
