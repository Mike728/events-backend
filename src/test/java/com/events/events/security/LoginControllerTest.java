package com.events.events.security;

import com.events.events.BasicTestConfig;
import com.events.events.common.TokenUtils;
import com.events.events.security.dto.LoginRequest;
import com.events.events.security.dto.LoginResponse;
import com.events.events.security.web.LoginController;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;

public class LoginControllerTest extends BasicTestConfig {

    @Autowired
    LoginController loginController;

    WebTestClient webTestClient;

    @Before
    public void init() {
        webTestClient = WebTestClient.bindToController(loginController)
            .build();
    }

    @Test
    public void loginTest() throws Exception {
        //given
        LoginRequest loginRequest = new LoginRequest("admin", "password");

        //when
        WebTestClient.ResponseSpec exchange = webTestClient
            .post()
            .uri("http://localhost:8080/api/login")
            .accept(MediaType.APPLICATION_JSON)
            .body(Mono.just(loginRequest), LoginRequest.class)
            .exchange();

        //then
        exchange.expectStatus().isOk();
        exchange.expectBody(LoginResponse.class);
        exchange.expectHeader().value("Authorization", header -> {
            String token = TokenUtils.extractToken(header);
            Assert.assertTrue(TokenUtils.validateToken(token));
        });
    }

    @Test
    public void introspectTest() {
        //given

        //when
        WebTestClient.ResponseSpec exchange = webTestClient
            .get()
            .uri("http://localhost:8080/api/introspect")
            .header("Authorization", TokenUtils.createTestToken())
            .accept(MediaType.APPLICATION_JSON)
            .exchange();

        //then
        exchange.expectStatus().isOk();
    }
}
