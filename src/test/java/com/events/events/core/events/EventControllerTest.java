package com.events.events.core.events;

import com.events.events.BasicTestConfig;
import com.events.events.model.entity.EventEntity;
import com.events.events.model.entity.EventLocation;
import com.events.events.model.entity.Organizer;
import com.events.events.model.enums.EventCategory;
import com.events.events.repository.EventRepository;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.UUID;

public class EventControllerTest extends BasicTestConfig {

    @Autowired
    EventController eventController;

    @Autowired
    EventRepository eventRepository;

    WebTestClient webTestClient;

    @Before
    public void init() {
        webTestClient = WebTestClient.bindToController(eventController)
            .build();
    }

    @Test
    public void findAllTest() throws Exception {
        //given

        //when
        WebTestClient.ResponseSpec exchange = webTestClient
            .get()
            .uri("http://localhost:8080/api/events")
            .accept(MediaType.APPLICATION_JSON)
            .exchange();

        Flux<EventEntity> responseBody = exchange.returnResult(EventEntity.class).getResponseBody();

        //then
        exchange.expectStatus().isOk();
        StepVerifier.create(responseBody)
            .expectNextCount(2)
            .expectComplete()
            .verify();
    }

    @Test
    public void filterByCategoryTest() throws Exception {
        //given
        final String category = "konferencja";

        //when
        WebTestClient.ResponseSpec exchange = webTestClient
            .get()
            .uri("http://127.0.0.1:8080/api/events?category=" + category)
            .accept(MediaType.APPLICATION_JSON)
            .exchange();

        Flux<EventEntity> responseBody = exchange.returnResult(EventEntity.class).getResponseBody();

        //then
        exchange.expectStatus().isOk();
        StepVerifier.create(responseBody)
            .expectNextMatches(event -> event.getCategory().getDescription().equalsIgnoreCase(category))
            .expectComplete()
            .verify();
    }

    @Test
    public void filterByNameAndDescriptionTest() throws Exception {
        //given
        final String name = "szkolenie";
        final String description = "facebook";

        //when
        WebTestClient.ResponseSpec exchange = webTestClient
                .get()
                .uri("http://127.0.0.1:8080/api/events?name=" + name + "&description=" + description)
                .accept(MediaType.APPLICATION_JSON)
                .exchange();

        Flux<EventEntity> responseBody = exchange.returnResult(EventEntity.class).getResponseBody();

        //then
        exchange.expectStatus().isOk();

        StepVerifier.create(responseBody)
                .expectNextMatches(event -> {
                    boolean nameCheck = event.getName().contains(name);
                    boolean descriptionCheck = event.getDescription().contains(description);

                    return nameCheck || descriptionCheck;
                })
                .expectComplete()
                .verify();
    }

    @Test
    public void saveNewEventEntityTest() throws Exception {
        //when
        webTestClient
                .post()
                .uri("http://127.0.0.1:8080/api/events")
                .syncBody(createTestEvent())
                .accept(MediaType.APPLICATION_JSON)
                .exchange();

        //then
        StepVerifier.create(eventRepository.findAll())
                .expectNextCount(3)
                .expectComplete()
                .verify();
    }

    private EventEntity createTestEvent() {
        Organizer organizer = Organizer.OrganizerBuilder.anOrganizer()
                .withCompanyName("Twitter")
                .withDescription("Serwis społecznościowy")
                .withDomain("https://www.twitter.com")
                .withEmail("facebook@facebook.com")
                .withFacebook("facebook")
                .withFirstName("Krzysztof")
                .withPhone("123-456-789")
                .withSurname("Murawski")
                .build();

        EventLocation eventLocation = EventLocation.EventLocationBuilder.anEventLocation()
                .withCity("Toronto")
                .withLocation("Ontario")
                .withStreetNumber("123a")
                .withStreet("Danforth street")
                .build();

        EventEntity eventEntity = EventEntity.EventEntityBuilder.anEventEntity()
                .withCategory(EventCategory.TRAINING)
                .withDateOfEvent(LocalDateTime.now().plusMonths(9))
                .withDescription("Szkolenie z twittera'a")
                .withEntriesAvailable(10)
                .withEntriesSold(33)
                .withEventUuid(UUID.randomUUID().toString())
                .withName("Szkolenie twitter")
                .withTags(Collections.unmodifiableList(Arrays.asList("twitter")))
                .withLocation(eventLocation)
                .withOrganizer(organizer)
                .build();

        return eventEntity;
    }

}
