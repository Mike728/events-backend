package com.events.events.core.registration;

import com.events.events.BasicTestConfig;
import com.events.events.repository.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.test.StepVerifier;

public class RegistrationControllerTest extends BasicTestConfig {

    @Autowired
    RegistrationController registrationController;

    @Autowired
    UserRepository userRepository;

    WebTestClient webTestClient;

    @Before
    public void init() {
        webTestClient = WebTestClient.bindToController(registrationController)
                .build();
    }

    @Test
    public void assertThatUserCanBeRegistered() throws Exception {
        RegistrationRequest registrationRequest = createRegistrationRequest();
        String email = registrationRequest.getEmail();

        //when
        WebTestClient.ResponseSpec exchange = webTestClient
                .post()
                .uri("http://127.0.0.1:8080/api/register")
                .syncBody(registrationRequest)
                .accept(MediaType.APPLICATION_JSON)
                .exchange();

        //then
        exchange.expectStatus().is2xxSuccessful();

        StepVerifier.create(userRepository.findByEmail(email))
                .expectNextMatches(user -> user.isActive() == true && !user.getRegistrationKey().isEmpty())
                .expectComplete()
                .verify();

        StepVerifier.create(exchange.returnResult(RegistrationResponse.class).getResponseBody().single())
                .expectNextMatches(response -> response.isActive() == false
                        && response.isRegistered() == true
                        && response.getEmail().equals(email))
                .expectComplete()
                .verify();
    }

    @Test
    public void assertThatUserWithExistingEmailCanNotBeRegistered() throws Exception {
        RegistrationRequest registrationRequest = createRegistrationRequestWithExistingEmail();
        String email = registrationRequest.getEmail();

        //when
        WebTestClient.ResponseSpec exchange = webTestClient
                .post()
                .uri("http://127.0.0.1:8080/api/register")
                .syncBody(registrationRequest)
                .accept(MediaType.APPLICATION_JSON)
                .exchange();

        //then
        exchange.expectStatus().is2xxSuccessful();

        StepVerifier.create(exchange.returnResult(RegistrationResponse.class).getResponseBody().single())
                .expectNextMatches(response -> response.isActive() == false
                        && response.isRegistered() == false
                        && response.getEmail().equals(email))
                .expectComplete()
                .verify();
    }
}
