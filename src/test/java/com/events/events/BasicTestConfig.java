package com.events.events;

import com.events.events.core.registration.RegistrationRequest;
import com.events.events.core.registration.RegistrationService;
import com.events.events.model.entity.EventEntity;
import com.events.events.model.entity.User;
import com.events.events.repository.EventRepository;
import com.events.events.repository.UserRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.testcontainers.containers.GenericContainer;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.Collections;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(initializers = {BasicTestConfig.Initializer.class})
public abstract class BasicTestConfig {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private UserRepository userRepository;

    private final Logger LOG = LoggerFactory.getLogger(RegistrationService.class);

    @Before
    public void setup() {
        clearDatabase();
        initializeTestUsers();
    }

    private void initializeTestUsers() {
        try {
            byte[] bytes = Files.readAllBytes(getFile("users.json").toPath());
            User user = objectMapper.readValue(bytes, User.class);
            userRepository.save(user).subscribe();

            byte[] eventsBytes = Files.readAllBytes(getFile("events.json").toPath());
            EventEntity[] eventEntities = objectMapper.readValue(eventsBytes, EventEntity[].class);
            eventRepository.saveAll(Arrays.asList(eventEntities)).subscribe();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void clearDatabase() {
        eventRepository.deleteAll().subscribe();
        userRepository.deleteAll().subscribe();
    }

    private File getFile(String filename) {
        try {
            return new ClassPathResource(filename).getFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        throw new IllegalArgumentException("Cannot read file");
    }

    static class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

        static final GenericContainer<?> mongoContainer;
        static final String CONTAINER_EXPOSED_PORT = "27017";
        static final String CONTAINER_NAME = "mongo:4.0";

        static {
            mongoContainer = new GenericContainer<>(CONTAINER_NAME)
                    .withExposedPorts(Integer.valueOf(CONTAINER_EXPOSED_PORT));
            mongoContainer.start();
        }

        @Override
        public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
            final String MONGO_MAPPED_PORT = "spring.data.mongodb.port=" + mongoContainer.getMappedPort(Integer.valueOf(CONTAINER_EXPOSED_PORT));

            TestPropertyValues of = TestPropertyValues.of(Collections.unmodifiableList(Arrays.asList(MONGO_MAPPED_PORT)));
            of.applyTo(configurableApplicationContext.getEnvironment());
        }
    }

    public RegistrationRequest createRegistrationRequest() {
        return RegistrationRequest.RegistrationRequestBuilder.aRegistrationRequest()
                .withEmail("test@test.pl")
                .withFirstName("Jan")
                .withLastName("Fachowiec")
                .withPassword("dupa8")
                .withRepeatPassword("dupa8")
                .build();
    }

    public RegistrationRequest createRegistrationRequestWithExistingEmail() {
        return RegistrationRequest.RegistrationRequestBuilder.aRegistrationRequest()
                .withEmail("admin")
                .withFirstName("Jan")
                .withLastName("Fachowiec")
                .withPassword("dupa8")
                .withRepeatPassword("dupa8")
                .build();
    }
}
